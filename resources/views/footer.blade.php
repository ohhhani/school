
<html>
 
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="footer, address, phone, icons" />
 
	<title>Home</title>
 
	<link rel="stylesheet" type="text/css" href="css/style.css">
	
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
 
	<link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
 
</head>
	<body>
        <header>
        <div id="main">
        <nav>
            <img src="images/logo.jpg" height="100" width="100"> 
            <ul>
                <li><a href="#">Home</a></li>
                <li><a href="#">About us</a></li>
                <li><a href="#">Admmission</a></li>
                <li><a href="#">Achievements</a></li>
                <li><a href="#">Courses</a></li>
                <li><a href="#">Scholarships</a></li>
                <li><a href="#">Contact</a></li>
            </ul>
        </nav>
        <img src="images/pic1.jpg" height="500" width="1350">
        </div>
        </header>

		<!-- The content of your page would go here. -->
 
		<footer class="footer-distributed">
 
			<div class="footer-left">
          <img src="images/logo.jpg">
				<h3>About<span>School</span></h3>
 
				<p class="footer-links">
					<a href="#">Home</a>
					|
					<a href="#">School Updates</a>
					|
					<a href="#">Gallery</a>
					|
					<a href="#">Contact</a>
				</p>
 
				<p class="footer-company-name">@ Bridgeford High School established in 1994</p>
			</div>
 
			<div class="footer-center">
				<div>
					<i class="fa fa-map-marker"></i>
					  <p><span>kishoreganj Rnchi road no=6 behind kumhar toli</p>
				</div>
 
				<div>
					<i class="fa fa-phone"></i>
					<p>+91 22-27782183</p>
				</div>
				<div>
					<i class="fa fa-envelope"></i>
					<p><a href="bridge:support@school.com">bridgeford@eduonix.com</a></p>
				</div>
			</div>
			<div class="footer-right">
				<p class="footer-company-about">
					<span>About the school</span>
					Affiliated to CBSE New Delhi, the school caters to pupils from Pre-primary to class XII in science and commerce stream and adheres strictly to the curriculum attained by CBSE.</p>
				<div class="footer-icons">
					<a href="#"><i class="fa fa-facebook"></i></a>
					<a href="#"><i class="fa fa-twitter"></i></a>
					<a href="#"><i class="fa fa-instagram"></i></a>
					<a href="#"><i class="fa fa-linkedin"></i></a>
					<a href="#"><i class="fa fa-youtube"></i></a>
				</div>
			</div>
		</footer>
	</body>
</html>